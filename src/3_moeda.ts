class Moeda {
    constructor(public valor: number, public nome: string){
    }
       
    get getvalor(): number {
        return this.valor;
    }

    get getnome(): string {
        return this.nome;
    }

    toJSON(): string {
        return JSON.stringify({
            valor: this.valor,
            nome: this.nome,
            
        })
    }

}

class Cofrinho {
    public _moeda: Moeda[] = [];

    adicionar(moeda: Moeda): void {
        this._moeda.push(moeda);
    }

    calcularTotal(): number {
        return this._moeda.reduce((soma, moeda) => soma + moeda.valor, 0);
    }

    getmenorMoeda(): number{
        let moeda = this.getMenorMoeda();
        return moeda ? moeda.valor : 0;
    }

    getMenorMoeda(): Moeda | null {
        return this._moeda.length ? this._moeda.sort((a, b) => a.valor - b.valor)[0] : null;
    }
}

let cofre = new Cofrinho();
cofre.adicionar(new Moeda(20, '20 reais'));
cofre.adicionar(new Moeda(10, '10 reais'))
let m1 = new Moeda(20, '20 reais');
let m2 = new Moeda(30, '30 reais');
console.log(cofre.calcularTotal());
console.log(cofre.getMenorMoeda());
