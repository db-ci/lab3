class Moeda {
    constructor(public valor: number, public nome: string){
    }
       
    get getvalor(): number {
        return this.valor;
    }

    get getnome(): string {
        return this.nome;
    }

    toJSON(): string {
        return JSON.stringify({
            valor: this.valor,
            nome: this.nome,
            
        })
    }

}

class Cofrinho {
    public _moeda: Moeda[] = [];

    adicionar(moeda: Moeda): void {
        this._moeda.push(moeda);
    }

    calcularTotal(): number {
        return this._moeda.reduce((soma, moeda) => soma + moeda.valor, 0);
    }
}

let cofre = new Cofrinho();
cofre.adicionar(new Moeda(20, '20 reais'));
cofre.adicionar(new Moeda(10, '10 reais'))
