

class Cliente {
    constructor(
        private _nome: string
    ) { }

    get nome(): string {
        return this._nome;
    }   

    abstract getMensalidade(): number
}

class ClienteFisico extends Cliente {
    constructor(nome: string, idade: number, salario: number) { 
        super(nome);
    }

    get idade(): number{
        return this.idade;
    }

    set idade(umaIdade: number) {
        this.idade = umaIdade;
    }

    get salario(): number{
        return this.salario;
    }

    set salario(umSalario: number) {
        this.salario = umSalario;
    }

    getMensalidade() {
        if (this.idade < 60) {
            return this.salario * 0, 10;
        } else
            return this.salario * 0, 15;
    }
}

class ClienteJuridico extends Cliente {
    constructor(nome: string, mensalidade: number) {
        super(nome);
    }

    set mensalidade(umaMensalidade: number){
        this.mensalidade = umaMensalidade;
    }

    calculaMensalidadeJuridico(umaMensalidade: number) {
        return umaMensalidade;
    }
}

let c1 = new Cliente('Cíntia');
let c2 = new ClienteFisico('Cíntia', 22, 2000);
console.log(c2.getMensalidade());
