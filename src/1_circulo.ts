class Circulo {
    constructor(
        private _x: number, 
        private _y: number, 
        private _raio: number
    ) {}

    getRaio(): number {
        return this._raio;
    }
    calculaArea(): number {
        return Math.PI * this._raio ** 2;
        
    }
    calculaComprimento(): number {
        return 2 * Math.PI * this._raio;
    }
}

let c = new Circulo(2, 2, 1);
console.log(c.calculaArea());
console.log(c.calculaComprimento());